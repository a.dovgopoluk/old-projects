﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sort_threads
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            typeof(Control).GetProperty("DoubleBuffered", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.SetProperty).SetValue(pictureBox1, true, null);
            typeof(Control).GetProperty("DoubleBuffered", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.SetProperty).SetValue(pictureBox2, true, null);
        }

        const int bubbleD = 50; //размер шариков для отрисовки
        const int gap = 10; //расстояние между шариками
        private List<int> sequense = new List<int>(); //последовательность чисел на входе
        private string[] splitSeq; //массив строк, в каждой ячейке которого число

        private List<Bubble> bubbles = new List<Bubble>(); //объекты-шарики полученные на старте работы программы, необходимые для отрисовки
        private List<Bubble> newbubbles = new List<Bubble>(); //новые отсортированные (сортируемые) шарики
        private static Random rand = new Random();
        private int delay; //задержка в циклах (для того, чтобы увидеть "движение" шариков)
        private bool check;

        Thread SSort, BSort, Count;

        private void button1_Click(object sender, EventArgs e)
        {
            Prestart();

            //в зависимости от радио-кнопки отсортировать одним или другим методом
            if (radioButton1.Checked == true)
            { 
                SSort = new Thread(new ThreadStart(SelectionStart));
                SSort.Start();
                check = true;                
            } 
            else if (radioButton2.Checked == true)
            {
                BSort = new Thread(new ThreadStart(BubbleStart));
                BSort.Start();
                check = false;
            }

            Count = new Thread(new ThreadStart(CountSort));
            Count.Start();

            //очистка предыдущих значений из рич текстбокса и вывод новых
            //richTextBox2.Clear();
            //foreach (var Num in newbubbles) richTextBox2.Text += Num.Value + " ";
        }

        private void CountSort()
        {
            if (check) newbubbles = Selection(bubbles, false);            
            else newbubbles = BubbleSort(bubbles, false);
            
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(this.CountSort));
            }
            else
            {
                richTextBox2.Clear();
                foreach (var Num in newbubbles) richTextBox2.Text += Num.Value + " ";
            }

            Count.Abort();
        }

        private void SelectionStart() //подготовка к сортировки методом выборки
        {
            DateTime date = DateTime.Now; //сохранение текущего времени
            //newbubbles = Selection(bubbles);
            Selection(bubbles, true);
            TimeSpan ts = date - DateTime.Now; //подсчёт интервала выполнения

            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(this.SelectionStart));
            }
            else
            {
                label1.Text = "Время выполнения и отрисовки: " + ts;
            }

            SSort.Abort();
        }

        private void BubbleStart() //подготовка к сортировки пузырьком
        {
            DateTime date = DateTime.Now; //сохранение текущего времени
            //newbubbles = BubbleSort(bubbles);
            BubbleSort(bubbles, true);
            TimeSpan ts = date - DateTime.Now; //подсчёт интервала выполнения

            //Label label2 = new Label();
            //label2.Text = "Время выполнения и отрисовки: " + ts;
            //label2.Size = new Size(label2.PreferredWidth, label2.PreferredHeight);
            //label2.Location = new Point(558, 430);
            //label2.Visible = true;
            //this.Controls.Add(label2);

            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(this.BubbleStart));
            }
            else
            {
                label2.Text = "Время выполнения и отрисовки: " + ts;
            }

            BSort.Abort();
        }

        private void Prestart() //подготовка к выполнению любой из сортировок
                                //и считывание значений
        {
            //очистка массивов и координат
            sequense.Clear();
            bubbles.Clear();
            Bubble.LastX = 0;
            Bubble.LastY = 0;

            try
            {
                foreach (var StrNum in splitSeq) //перевод в инт
                {
                    sequense.Add(System.Convert.ToInt32(StrNum));
                }

                foreach (var item in sequense) //формирование массива шариков
                {
                    //если новый шарик выйдет за рамки пикчербокса изменим координату по Y
                    if ((pictureBox1.Size.Width) <= Bubble.LastX + bubbleD) { Bubble.LastY += gap + bubbleD; Bubble.LastX = 0; }
                    //добавление нового шарика
                    bubbles.Add(new Bubble(item, Bubble.LastX, Bubble.LastY, Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256))));
                    //изменение координаты X для будущего шара
                    Bubble.LastX += gap + bubbleD;
                }
            }
            catch (Exception e) //если что-то пошло не так
            {
                MessageBox.Show(e.Message, "Ошибка");
            }
        }

        private List<Bubble> Selection(List<Bubble> list, bool draw) //сортировка выборкой
        {
            List<Bubble> tempBubbles = new List<Bubble>();
            foreach (var item in list)
            {
                tempBubbles.Add(new Bubble(item));
            }

            for (int i = 0; i < tempBubbles.Count - 1; i++) //пробегаемся по массиву шариков
            {
                int min = i; //индекс самого минимального значения
                for (int j = i + 1; j < tempBubbles.Count; j++)
                {
                    if (tempBubbles[j].Value < tempBubbles[min].Value) //если значение меньше минимального сохраняем
                    {
                        min = j;
                    }
                }
                //меняем местами текущее значение и минимальное
                Bubble temp = tempBubbles[i];
                tempBubbles[i] = tempBubbles[min];
                tempBubbles[min] = temp;

                //меняем местами координаты соответствующих шариков
                temp = new Bubble(tempBubbles[i]);
                tempBubbles[i].X = tempBubbles[min].X;
                tempBubbles[i].Y = tempBubbles[min].Y;
                tempBubbles[min].X = temp.X;
                tempBubbles[min].Y = temp.Y;

                if (checkBox1.Checked && draw) //если стоит галочка отрисовка - рисуем
                    Draw(tempBubbles);
                if (draw)
                    Thread.Sleep(delay); //задержка
            }
            return tempBubbles;
        }

        private List<Bubble> BubbleSort(List<Bubble> list, bool draw) //сортировка пузырьком
        {
            Bubble temp;
            List<Bubble> tempBubbles = new List<Bubble>();
            foreach (var item in list)
            {
                tempBubbles.Add(new Bubble(item));
            }

            for (int i = 0; i < tempBubbles.Count - 1; i++)
            {
                for (int j = 0; j < tempBubbles.Count - 1 - i; j++)
                {
                    if (tempBubbles[j + 1].Value < tempBubbles[j].Value) //если правое значение больше левого
                    {
                        //меняем местами
                        temp = tempBubbles[j + 1];
                        tempBubbles[j + 1] = tempBubbles[j];
                        tempBubbles[j] = temp;

                        //меняем местами координаты соответствующих шариков
                        temp = new Bubble(tempBubbles[j + 1]);
                        tempBubbles[j + 1].X = tempBubbles[j].X;
                        tempBubbles[j + 1].Y = tempBubbles[j].Y;
                        tempBubbles[j].X = temp.X;
                        tempBubbles[j].Y = temp.Y;
                    }

                    if (checkBox1.Checked && draw) //если стоит галочка отрисовка - рисуем
                        Draw2(tempBubbles);
                    if (draw)
                        Thread.Sleep(delay); //задержка
                }
            }
            return tempBubbles;
        }

        private void Draw(List<Bubble> list) //отрисовка для метода выборки
        {

            Graphics g = pictureBox1.CreateGraphics(); //создание "места" для рисования
            g.Clear(Color.White); //очистка
            Font f = richTextBox1.Font; //выбор шрифта для надписи на шариках

            foreach (var bubble in list) //для каждого шарика
            {
                SolidBrush b = new SolidBrush(bubble.Color); //создаём кисть соответствующего цвета
                //SolidBrush b2 = new SolidBrush(Color.FromArgb(255 - bubble.Color.R, 255 - bubble.Color.G, 255 - bubble.Color.B)); //создаём кисть противоположного цвета
                SolidBrush b2 = new SolidBrush(Color.Black); //создаём кисть черного цвета
                g.FillEllipse(b, bubble.X, bubble.Y, bubbleD, bubbleD); //рисуем шарик
                g.DrawString(bubble.Value.ToString(), f, b2, bubble.X + bubbleD / 2 - (f.Size / 2 * bubble.Value.ToString().Length), bubble.Y + bubbleD / 2 - f.Height / 2); //подписываем шарик
            }
        }

        private void Draw2(List<Bubble> list) //отрисовка для пузырька (аналогична предыдущей)
        {
            Graphics g = pictureBox2.CreateGraphics();
            g.Clear(Color.White);
            Font f = richTextBox1.Font;

            foreach (var bubble in list)
            {
                SolidBrush b = new SolidBrush(bubble.Color);
                //SolidBrush b2 = new SolidBrush(Color.FromArgb(255 - bubble.Color.R, 255 - bubble.Color.G, 255 - bubble.Color.B));
                SolidBrush b2 = new SolidBrush(Color.Black);
                g.FillEllipse(b, bubble.X, bubble.Y, bubbleD, bubbleD);
                g.DrawString(bubble.Value.ToString(), f, b2, bubble.X + bubbleD / 2 - (f.Size / 2 * bubble.Value.ToString().Length), bubble.Y + bubbleD / 2 - f.Height / 2);
            }
        }

        public string[] RemoveZero(string[] Split) //убирает лишние значения                                          
        {                                          //(при вводе нескольких пробелов подряд)
            string[] NewSplit = new string[0];
            int j = 0;
            for (int i = 0; i < Split.Length; i++)
            {
                if (Split[i] != "")
                {
                    Array.Resize(ref NewSplit, j + 1);
                    NewSplit[j] = Split[i];
                    j++;
                }
            }
            return NewSplit;
        }

        private void IsNumber(string[] Split) //проверка на то, что все введённые значения - числа
        {
            List<string> Numbers = new List<string>();
            foreach (var Numb in Split)
            {
                if (!int.TryParse(Numb, out int res)) throw new Exception();
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e) //при изменении теста в верхнем рич текстбоксе
        {
            if (radioButton1.Checked || radioButton2.Checked) button1.Enabled = true; //включаем кнопку если выбрано всё что нужно
            richTextBox1.BackColor = Color.White; //фон рич тестбокса красим белым
            try
            {
                //пытаемся разделить всё на числа и преобразовать в инт
                splitSeq = richTextBox1.Text.Split(' ');
                splitSeq = RemoveZero(splitSeq);
                IsNumber(splitSeq);
            }
            catch (Exception) //если что-то пошло не так
            {
                richTextBox1.BackColor = Color.Red; //фон рич тестбокса красим красным
                button1.Enabled = false; //не даём нажать на кнопку
            }
        }

        private void button2_Click(object sender, EventArgs e) //показываем работу сразу двух алгоритмов
        {
            Prestart();
            SSort = new Thread(new ThreadStart(SelectionStart));
            BSort = new Thread(new ThreadStart(BubbleStart));
            SSort.Start();
            BSort.Start();

            richTextBox2.Clear();
            foreach (var Num in newbubbles) richTextBox2.Text += Num.Value + " ";
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e) //изменение задержки
        {
            delay = (int)numericUpDown1.Value;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e) //включение кнопок
        {
            button1.Enabled = true;
            button2.Enabled = true;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                Count.Abort();
                SSort.Abort();
                BSort.Abort();
            }
            catch { }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e) //включение кнопок
        {
            button1.Enabled = true;
            button2.Enabled = true;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e) //вкл/выкл изменения задержки
        {
            if (checkBox1.Checked) numericUpDown1.Enabled = true;
            else
            {
                numericUpDown1.Value = 0;
                numericUpDown1.Enabled = false;
            }
        }
    }
}
