﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace sort_threads
{
    class Bubble //класс шариков
    {
        private int value; //число, которое представляет собой
        private int x, y; //кординаты отрисовки
        private Color color; //цвет отрисовки
        private static int lastX = 0, lastY = 0; //координаты для отрисовки нового шарика

        public Bubble(int value, int x, int y, Color color) //конструктор создания
        {
            this.value = value;
            this.x = x;
            this.y = y;
            this.color = color;
        }

        public Bubble(Bubble b) //конструктор копирования значений другого шарика
        {
            this.value = b.value;
            this.x = b.x;
            this.y = b.y;
            this.color = b.color;
        }

        //getters and setters
        public int Value { get => value; set => this.value = value; }
        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }
        public Color Color { get => color; set => color = value; }
        public static int LastX { get => lastX; set => lastX = value; }
        public static int LastY { get => lastY; set => lastY = value; }
    }
}
